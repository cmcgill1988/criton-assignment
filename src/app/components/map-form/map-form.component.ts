import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from '@types/googlemaps';

@Component({
  selector: 'map-form',
  templateUrl: './map-form.component.html',
  styleUrls: ['./map-form.component.scss']
})
export class MapFormComponent implements OnInit {
  @ViewChild('locationSearch') public locationSearchElement: ElementRef;
  searchInput: FormControl = new FormControl('');
  autocomplete: google.maps.places.Autocomplete;
  latitude: number;
  longitude: number;
  noLocationFound: boolean;

  constructor(public mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.clearPlace();
  }

  ngOnInit() {
    this.loadMapsAPI();
  }

  getPlace() {
    const place: google.maps.places.PlaceResult = this.autocomplete.getPlace();

    if (place && this.searchInput.value.length > 0) {
      this.setPlace(place);
    } else {
      this.clearPlace();
    }
  }

  loadMapsAPI() {
    this.mapsAPILoader.load().then(() => {
      this.autocomplete = new google.maps.places.Autocomplete(this.locationSearchElement.nativeElement,
        {
          types: ['address']
        });

      this.autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          this.getPlace();
        });
      });
    }).catch(() => {
      console.error('Failed to load google maps api');
    });
  }

  shouldShowAlert() {
    if (this.searchInput.value.length > 0 && !this.searchInput.pristine) {
      return this.noLocationFound;
    }
    return false;
  }

  private clearPlace() {
    this.noLocationFound = true;
    this.latitude = this.longitude = null;
  }

  private setPlace(place) {
      if (place.geometry === undefined || place.geometry === null) {
        this.clearPlace();
      } else {
        this.noLocationFound = false;
        this.latitude = place.geometry.location.lat();
        this.longitude = place.geometry.location.lng();
      }
  }
}
