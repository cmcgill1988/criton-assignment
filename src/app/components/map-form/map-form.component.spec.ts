/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MapFormComponent } from './map-form.component';
import { MapsAPILoader, AgmCoreModule } from '@agm/core';
import { Environment } from '../../environment';
import { ReactiveFormsModule } from '@angular/forms';

describe('MapFormComponent', () => {
  let component: MapFormComponent;
  let fixture: ComponentFixture<MapFormComponent>;
  let searchInputElement: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapFormComponent],
      imports: [
        AgmCoreModule.forRoot(Environment.googleMapsApi),
        ReactiveFormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const loader: MapsAPILoader = TestBed.get(MapsAPILoader);
    loader.load();

    searchInputElement = component.locationSearchElement.nativeElement;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should call loadMapsAPI once', () => {
      spyOn(component, 'loadMapsAPI');
      component.ngOnInit();
      expect(component.loadMapsAPI).toHaveBeenCalledTimes(1);
    });

    it('should set value of noLocationFound to true initially', () => {
      expect(component.noLocationFound).toBeTruthy();
    });

    it('should set value of latitude to null initially', () => {
      expect(component.latitude).toBe(null);
    });

    it('should set value of longitude to null initially', () => {
      expect(component.longitude).toBe(null);
    });
  });

  describe('loadMapsAPI', () => {
    beforeEach(() => {
      component.autocomplete = new google.maps.places.Autocomplete(component.locationSearchElement.nativeElement,
        {
          types: ['address']
        });
    });

    it('should define autocomplete', () => {
      component.mapsAPILoader.load().then(() => {
        expect(component.autocomplete).toBeDefined();
      });
    });

    it('should call autocomplete.addListener', () => {
      spyOn(component.autocomplete, 'addListener');
      component.mapsAPILoader.load().then(() => {
        expect(component.autocomplete.addListener).toHaveBeenCalledTimes(1);
      });
    });

    it('should call console.error once on failing to load mapsAPILoader', () => {
      spyOn(console, 'error');
      component.mapsAPILoader.load().catch(() => {
        expect(console.error).toHaveBeenCalledTimes(1);
        expect(console.error).toHaveBeenCalledWith('Failed to load google maps api');
      });
    });

    it('should call console.error on failing to load mapsAPILoader with Failed to load google maps api', () => {
      spyOn(console, 'error');
      component.mapsAPILoader.load().catch(() => {
        expect(console.error).toHaveBeenCalledWith('Failed to load google maps api');
        expect(component.autocomplete.addListener).toBeUndefined();
      });
    });

    it('should call not set autocomplete.addListener on failing to load mapsAPILoader', () => {
      component.mapsAPILoader.load().catch(() => {
        expect(component.autocomplete.addListener).toBeUndefined();
      });
    });
  });

  describe('shouldShowAlert', () => {
    it('should return false when searchInput is empty and is pristine', () => {
      expect(component.shouldShowAlert()).toBeFalsy();
    });

    it('should return false when searchInput length === 0 and !pristine', () => {
      component.searchInput.markAsDirty();
      expect(component.shouldShowAlert()).toBeFalsy();
    });

    it('should return true when searchInput length > 0, !pristine and noLocationFound === true', () => {
      component.searchInput.setValue('test');
      component.searchInput.markAsDirty();
      expect(component.shouldShowAlert()).toBeTruthy();
    });

    it('should return false when searchInput length > 0, !pristine and noLocationFound === false', () => {
      component.searchInput.setValue('test');
      component.searchInput.markAsDirty();
      component.noLocationFound = false;
      expect(component.shouldShowAlert()).toBeFalsy();
    });
  });

  describe('getPlace', () => {
    beforeEach(() => {
      component.autocomplete = new google.maps.places.Autocomplete(component.locationSearchElement.nativeElement,
        {
          types: ['address']
        });
      spyOn(component.autocomplete, 'getPlace');
    });

    it('should call autocomplete.getPlace once', () => {
      component.getPlace();
      expect(component.autocomplete.getPlace).toHaveBeenCalledTimes(1);
    });

    describe('when no result is returned', () => {
      beforeEach(() => {
        component.noLocationFound = false;
        component.longitude = 1;
        component.latitude = 2;

        component.searchInput.setValue('');
        searchInputElement.dispatchEvent(new Event('focusout'));
      });
      it('should set noLocationFound to true if search does not return a result', () => {
        expect(component.noLocationFound).toBeTruthy();
      });

      it('should set longitude to null if no result returned', () => {
        expect(component.longitude).toBe(null);
      });

      it('should set latitude to null if no result returned', () => {
        expect(component.latitude).toBe(null);
      });
    });
  });

  describe('autocomplete listener', () => {
    describe('place_changed event', () => {
      beforeEach(() => {
        component.autocomplete = new google.maps.places.Autocomplete(component.locationSearchElement.nativeElement,
          {
            types: ['address']
          });

        component.autocomplete.addListener('place_changed', () => {
          this.getPlace();
        });

        spyOn(component, 'getPlace');
      });

      it('should call getPlace on lose focus event from searchInput', () => {
        component.searchInput.setValue('Tester Road', { emitEvent: true, emitModelToViewChange: true });
        searchInputElement.dispatchEvent(new Event('focusout'));

        expect(component.getPlace).toHaveBeenCalledTimes(1);
      });
    });
  });
});
