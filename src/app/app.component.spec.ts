import { MapFormComponent } from './components/map-form/map-form.component';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { Environment } from './environment';
import { ReactiveFormsModule } from '@angular/forms';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, MapFormComponent
      ],
      imports: [
        AgmCoreModule.forRoot(Environment.googleMapsApi),
        ReactiveFormsModule
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
