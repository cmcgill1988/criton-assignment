import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

// Third party modules
import { AgmCoreModule } from '@agm/core';

// Imported components
import { AppComponent } from './app.component';
import { MapFormComponent } from './components/map-form/map-form.component';

// Imported Environmental constants
import { Environment } from './environment';

@NgModule({
  declarations: [
    AppComponent,
    MapFormComponent
  ],
  imports: [
    AgmCoreModule.forRoot(Environment.googleMapsApi),
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
